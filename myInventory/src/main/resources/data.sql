insert into vehicle (vehicle_transport_type, manufacturer, model, fuel_type, color, odometer_reading, fuel_capacity, fuel_threshold_percent,  description) values ('RoadVehicle', 'TESLA', 'ModelX', 'blue',  'electric', 25, 0, 100, 'my TESLA ModelX');
insert into vehicle (vehicle_transport_type, manufacturer, model, fuel_type, color, odometer_reading, fuel_capacity, fuel_threshold_percent,  description) values ('RoadVehicle', 'BMW', '320i', 'black',  'gas', 11352, 20, 75, 'my BMW 320i');
insert into vehicle (vehicle_transport_type, manufacturer, model, fuel_type, color, odometer_reading, fuel_capacity, fuel_threshold_percent,  description) values ('RoadVehicle', 'HONDA', 'Odyssey', 'silver',  'gas', 56982, 25, 35, 'my Honda Odyssey');
insert into vehicle (vehicle_transport_type, manufacturer, model, fuel_type, color, odometer_reading, fuel_capacity, fuel_threshold_percent,  description) values ('RoadVehicle', 'HONDA', 'Accord', 'white',  'diesel', 25, 0, 100, 'my Honda Accord Diesel');
insert into vehicle (vehicle_transport_type, manufacturer, model, fuel_type, color, odometer_reading, fuel_capacity, fuel_threshold_percent,  description) values ('RoadVehicle', 'VOLKSWAGON', 'Passat', 'red',  'hybrid', 25, 0, 100, 'my VW passatt Hybrid');

commit; 

