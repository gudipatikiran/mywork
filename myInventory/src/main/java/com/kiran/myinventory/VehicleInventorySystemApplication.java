package com.kiran.myinventory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.kiran.myinventory.dao.VehicleDao;
import com.kiran.myinventory.domain.Vehicle;

@SpringBootApplication
public class VehicleInventorySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleInventorySystemApplication.class, args);
		
	}
	
	/* 
   @Bean  
	public CommandLineRunner demo(VehicleRepository repository) {
		return (args) -> {
			// fetch all customers
			System.out.println("Customers found with findAll():");
			System.out.println("-------------------------------");
			for (Vehicle customer : repository.findAll()) {
				System.out.println(customer.toString());
			}
			System.out.println("");
		};
	}
	*/

}
