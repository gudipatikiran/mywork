package com.kiran.myinventory.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kiran.myinventory.domain.Vehicle;
import com.kiran.myinventory.domain.VehicleTransportType;

public interface VehicleDao extends CrudRepository<Vehicle, Long> {

    List<Vehicle> findByManufacturer(String manufacturer);
    List<Vehicle> findByModel(String model);
    List<Vehicle> findByColor(String color);
    List<Vehicle> findByFuelType(String fuelType);
    List<Vehicle> findByvehicleTransportType(VehicleTransportType vehicleTransportType); 

}