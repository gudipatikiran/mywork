package com.kiran.myinventory.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "RoadVehicle")
@PrimaryKeyJoinColumn(name = "vehicleId")
public class RoadVehicle extends Vehicle{
	private String VehicleIdentificationNumber;
	private long haulageCapacity;
	private String haulageCapacityUOM; 
}
