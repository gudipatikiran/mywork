package com.kiran.myinventory.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;

@Entity
@Table(name = "TestDriveLog")
public class TestDriveLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long testDriveId;
	//Vehicle associated with this test drive
    @ManyToOne
    @JoinColumn(name="vehicle_id", nullable = false)
    private Vehicle vehicle;
    //customer associated with this test drive
    @ManyToOne
    @JoinColumn(name="customer_id", nullable = false)    
	private Customer customer;
	public long getTestDriveId() {
		return testDriveId;
	}
	public void setTestDriveId(long testDriveId) {
		this.testDriveId = testDriveId;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Date getTestDriveStartTime() {
		return testDriveStartTime;
	}
	public void setTestDriveStartTime(Date testDriveStartTime) {
		this.testDriveStartTime = testDriveStartTime;
	}
	public Date getTestDriveEndTime() {
		return testDriveEndTime;
	}
	public void setTestDriveEndTime(Date testDriveEndTime) {
		this.testDriveEndTime = testDriveEndTime;
	}
	private Date testDriveStartTime;
	private Date testDriveEndTime;
	

}
