package com.kiran.myinventory.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DealerLocation")
public class DealerLocation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long dealerLocationId; 
}
