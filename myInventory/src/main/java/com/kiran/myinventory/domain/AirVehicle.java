package com.kiran.myinventory.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "AirVehicle")
@PrimaryKeyJoinColumn(name = "vehicleId")
public class AirVehicle extends Vehicle{
	private String TailNumber; 
}
