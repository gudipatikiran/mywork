package com.kiran.myinventory.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "WaterVehicle")
@PrimaryKeyJoinColumn(name = "vehicleId")
public class WaterVehicle extends Vehicle{
	private String HullIdentificationNumber;
	private Long haulageCapacity;
	private String haulageCapacityUOM; 
}
