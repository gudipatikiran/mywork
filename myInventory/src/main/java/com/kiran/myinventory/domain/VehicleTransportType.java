package com.kiran.myinventory.domain;
import static java.util.Arrays.asList;
 
public enum VehicleTransportType {
        RoadVehicle(RoadVehicle.class),
        WaterVehicle(WaterVehicle.class),
        AirVehicle(AirVehicle.class);
        
        private final Class<? extends Vehicle> vehicleClass;
 
        VehicleTransportType(Class<? extends Vehicle> vehicleClass) {
            this.vehicleClass = vehicleClass;
        }
 
        public Class<? extends Vehicle> getvehicleClass() {
            return vehicleClass; 
        }
 
        public static VehicleTransportType get(Class<? extends Vehicle> vehicleClass) {
            return asList(values()).stream()
                    .filter(type -> type.vehicleClass.equals(vehicleClass))
                    .findFirst().orElseThrow(() -> new IllegalArgumentException(
                                                "Invalid Vehicle class"));
        }
 
        public String getExternalName() {
            return vehicleClass.getSimpleName();
        }
}