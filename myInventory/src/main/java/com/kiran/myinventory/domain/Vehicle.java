package com.kiran.myinventory.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "Vehicle")
public class Vehicle { 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long vehicleId;
	@Column(name = "manufacturer")
	private String manufacturer;
	
	@Column(name = "model")
	private String model;
	
	@Column(name = "color")
	private String color;

	@Column(name = "odometer_reading")
	private Long odometerReading;
	
	@Column(name = "fuel_type")
	private String fuelType;
	
	@Column(name = "description")
	private String description;

	@Column(name = "vehicle_transport_type")
	@Enumerated(EnumType.STRING)
	private VehicleTransportType vehicleTransportType;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getOdometerReading() {
		return odometerReading;
	}

	public void setOdometerReading(Long odometerReading) {
		this.odometerReading = odometerReading;
	}

	public String getFuleType() {
		return fuelType;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public void setFuleType(String fuleType) {
		this.fuelType = fuleType;
	}

	public int getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	private int fuelCapacity;
	private Integer fuelThresholdPercent;

	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public VehicleTransportType getVehicleTransportType() {
		return vehicleTransportType;
	}

	public void setVehicleTransportType(VehicleTransportType vehicleTransportType) {
		this.vehicleTransportType = vehicleTransportType;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Long vehicleID) {
		this.vehicleId = vehicleID;
	}

	public Integer getFuelThresholdPercent() {
		return fuelThresholdPercent;
	}

	public void setFuelThresholdPercent(Integer fuelThresholdPercent) {
		this.fuelThresholdPercent = fuelThresholdPercent;
	}


}
