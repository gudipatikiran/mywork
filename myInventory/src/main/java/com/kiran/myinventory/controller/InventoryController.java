package com.kiran.myinventory.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.kiran.myinventory.domain.Vehicle;
import com.kiran.myinventory.service.VehicleService;

@RestController
public class InventoryController {
	
	@Autowired
	private VehicleService vehicleService;
	
	@GetMapping(value="/vehicle/{vehicleId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Vehicle getAllVehicleById(@PathVariable(value="vehicleId") long vehicleId) {
		return vehicleService.findVehiclById(vehicleId);
	}
	
	@GetMapping(value="/vehicle", produces = MediaType.APPLICATION_JSON_VALUE) 
	public List<Vehicle> getAllVehicles() {
		return vehicleService.findAllVehicles();
	}

	@RequestMapping(value="/vehicle/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Vehicle> searchVehicleByManufacturer(@RequestParam(value="manufacturer") String manufacturer) {
		return vehicleService.findVehicleByManufacturer(manufacturer);
	}

}
