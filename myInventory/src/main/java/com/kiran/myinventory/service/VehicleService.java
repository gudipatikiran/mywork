package com.kiran.myinventory.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kiran.myinventory.dao.VehicleDao;
import com.kiran.myinventory.domain.Vehicle;

@Service
public class VehicleService {
	
	@Autowired
	private VehicleDao vehicleDao;

	public Vehicle findVehiclById(Long vehicleId) {
		System.out.println("calling findVehiclById DAO : vehicleId : "+vehicleId);

		Optional<Vehicle> vehicle = vehicleDao.findById(vehicleId);		
		return vehicle.get();
	} 

	public List<Vehicle> findAllVehicles(){
		System.out.println("calling findAllVehicles DAO");

		List<Vehicle> vehicleList = new ArrayList<>();
				
		for (Vehicle vehicle : vehicleDao.findAll()) {
			vehicleList.add(vehicle);
			//System.out.println(vehicle.toString());
		}
		System.out.println("Completed Servic Call");
		return vehicleList;
	}

	public List<Vehicle> findVehicleByManufacturer(String manufacturer) {
		List<Vehicle> vehicleList = new ArrayList<>();

		System.out.println("calling findByManufacturer DAO : manufacturer : "+manufacturer);
		for (Vehicle vehicle : vehicleDao.findByManufacturer(manufacturer)) {
			vehicleList.add(vehicle);
			//System.out.println(vehicle.toString());
		}
		System.out.println("Completed Servic Call");
		return vehicleList;	
	}

	public List<Vehicle> findVehicleByModel(String model) {
		List<Vehicle> vehicleList = new ArrayList<>();

		System.out.println("calling findByModel DAO : model : "+model);
		for (Vehicle vehicle : vehicleDao.findByManufacturer(model)) {
			vehicleList.add(vehicle);
			//System.out.println(vehicle.toString());
		}
		System.out.println("Completed Servic Call");
		return vehicleList;	
	}
	
	public List<Vehicle> findVehicleByColor(String color) {
		List<Vehicle> vehicleList = new ArrayList<>();

		System.out.println("calling findByColor DAO : color : "+color);
		for (Vehicle vehicle : vehicleDao.findByManufacturer(color)) {
			vehicleList.add(vehicle);
			//System.out.println(vehicle.toString());
		}
		System.out.println("Completed Servic Call");
		return vehicleList;	
	}

	public List<Vehicle> findVehicleByFuelType(String fuelType) {
		List<Vehicle> vehicleList = new ArrayList<>();

		System.out.println("calling findByManufacturer DAO : fuelType : "+fuelType);
		for (Vehicle vehicle : vehicleDao.findByManufacturer(fuelType)) {
			vehicleList.add(vehicle);
			//System.out.println(vehicle.toString());
		}
		System.out.println("Completed Servic Call");
		return vehicleList;	
	}
	
	public List<Vehicle> findByVehicleTransportType(String vehicleTransportType) {
		List<Vehicle> vehicleList = new ArrayList<>();

		System.out.println("calling findByManufacturer DAO : vehicleTransportType : "+vehicleTransportType);
		for (Vehicle vehicle : vehicleDao.findByManufacturer(vehicleTransportType)) {
			vehicleList.add(vehicle);
			//System.out.println(vehicle.toString());
		}
		System.out.println("Completed Servic Call");
		return vehicleList;	
	}

}
